<?php
/**
 * @author Grzegorz Olszewski <grzegorz@olszewski.in>
 */
class LetterMagic
{
    

	public $sizeofarray,$newtext,$words,$firstword,$secondword,$newfirst,$final_result;

    
    public function reverseString($text)
	{
		
		
		$this->sizeofarray=strlen($this->text);	//get the length of a word
		$j=1;

		for ($i=$this->sizeofarray-1;$i>=0;$i--)
			{
				$this->newtext[$j]=$this->text[$i];
				$j++;
			}

        	$this->final_result=implode($this->newtext); //connect the array to string
			return $this->final_result;
    	
	}
	public function reverseFirst($text)
	{
		$this->words=explode(' ',$this->text);
		$this->secondword=$this->words[1];
		$this->firstword=$this->words[0];
		
		$this->sizeofarray=strlen($this->firstword);
		$l=1;
		
		for ($k=$this->sizeofarray;$k>=0;$k--)
		{
			$this->newfirst[$l]=$this->text[$k];
			$l++;
		}
		
		if(is_array($this->newfirst)==true) $this->newfirst = implode($this->newfirst);
		$this->final_result= $this->newfirst.' '.$this->secondword;
		return $this->final_result;
    
	}
	public function reverseEvery($text)
	{
		$this->words=explode(' ',$this->text);	//split to words
		$this->words=array_filter($this->words); //get rid of empty array entries
		$this->words=array_values($this->words); //get array keys right after filtering

	
		for ($words_iterator=0;$words_iterator<count(array_keys($this->words));$words_iterator++)	//iterating through words
		{
			$this->sizeofarray=strlen($this->words[$words_iterator]);
			$l=1;
			
			for ($k=$this->sizeofarray-1;$k>=0;$k--)
			{
				$this->newwords[$words_iterator][$l]=$this->words[$words_iterator][$k];
				$l++;
			}
		}
			
		for ($words_iterator=0;$words_iterator<count(array_keys($this->newwords));$words_iterator++)
		{
			if(is_array($this->newwords[$words_iterator])==true) $this->newwords[$words_iterator] = implode($this->newwords[$words_iterator]);
		}
		
		if(is_array($this->newwords))
		{
			foreach ($this->newwords as &$value)
			$value = $value.' ';		//adding spaces between new words
			$this->newwords = implode(array_reverse($this->newwords));
		}
		

		return $this->newwords;
		
	}

}
?>
