# LetterMagic

LetterMagic is a PHP library created to manipulate strings.
You can:
  - Reverse string (Hello World -> dlroW olleH)
  - Reverse letters in the first word (olleH World)
  - Combine options (World olleH)

### Usage

Usage example in index.php. 

```php
require_once('lettermagiclib.php');

$text="Hello World";
echo "TEXT: ".$text.PHP_EOL;
$t = new LetterMagic;
$t->text=$text;

echo "REVERSE STRING:";

echo $t -> reverseString($t->text);
echo PHP_EOL;
```



### Version
0.0.1



### Development
If you want you can expand the project. Take a look at lettermagiclib.php and add your function :)

### Todo's

 - Add more comments
 - Better implementation of reverse first word
 
License
----

Apache v 2.0


